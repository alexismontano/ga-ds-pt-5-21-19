{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://imgur.com/1ZcRyrc.png\" style=\"float: left; margin: 20px; height: 55px\">\n",
    "\n",
    "# Linear Regression\n",
    "\n",
    "_Authors: Kevin Markham (Washington, D.C.), Ed Podojil (New York City)_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Learning Objectives\n",
    "- Define data modeling and simple linear regression.\n",
    "- Build a linear regression model using a data set that meets the linearity assumption using the scikit-learn library.\n",
    "- Understand and identify multicollinearity in a multiple regression."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Lesson Guide\n",
    "- [Bikeshare Data Set](#introduce-the-bikeshare-dataset)\n",
    "- [Using the Model for Prediction](#using-the-model-for-prediction) \n",
    "- [Bonus Material: Regularization](#bonus-material-regularization)\n",
    "\t- [How Does Regularization Work?](#how-does-regularization-work)\n",
    "\t- [Lasso and Ridge Path Diagrams](#lasso-and-ridge-path-diagrams)\n",
    "\t- [Advice for Applying Regularization](#advice-for-applying-regularization)\n",
    "\t- [Ridge Regression](#ridge-regression)\n",
    "- [Comparing Linear Regression With Other Models](#comparing-linear-regression-with-other-models)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import seaborn as sns\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "plt.rcParams['figure.figsize'] = (8, 6)\n",
    "plt.rcParams['font.size'] = 14\n",
    "plt.style.use(\"fivethirtyeight\")\n",
    "\n",
    "from sklearn.model_selection import train_test_split"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"read-in-the--capital-bikeshare-data\"></a>\n",
    "### Read In the Capital Bikeshare Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Read the data and set the datetime as the index.\n",
    "url = 'data/bikeshare.csv'\n",
    "bikes = pd.read_csv(url, index_col='datetime', parse_dates=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that we used `index_col` to set an index or primary key for our data. In this case, the index of each row will be set to the value of its `datetime` field.\n",
    "\n",
    "We also ask Pandas to parse dates (if `parse_dates=True`, for the index only). So, rather than reading in a string, Pandas converts the index string to a `datetime` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Preview the first five rows of the DataFrame.\n",
    "bikes.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"building-a-linear-regression-model-in-sklearn\"></a>\n",
    "### Building a Linear Regression Model in sklearn"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Create a feature matrix called \"X\" that holds a `DataFrame` with only the \"temp\" variable and a `Series` called y that has the \"total_rentals\" column."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Use the .rename() method to rename count to total.\n",
    "bikes.rename(columns={'count':'total_rentals'}, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Create X and y.\n",
    "feature_cols = ['temp']\n",
    "X = bikes[feature_cols]\n",
    "y = bikes.total_rentals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"scikit-learns--step-modeling-pattern\"></a>\n",
    "### scikit-learn's Four-Step Modeling Pattern"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 1:** Import the class you plan to use."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from sklearn.linear_model import LinearRegression"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 2:** \"Instantiate\" the \"estimator.\"\n",
    "\n",
    "- \"Estimator\" is scikit-learn's term for \"model.\"\n",
    "- \"Instantiate\" means \"make an instance of.\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Make an instance of a LinearRegression object.\n",
    "lr = LinearRegression()\n",
    "type(lr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Created an object that \"knows\" how to do linear regression, and is just waiting for data.\n",
    "- Name of the object does not matter.\n",
    "- All parameters not specified are set to their defaults.\n",
    "- Can specify tuning parameters (aka \"hyperparameters\") during this step. \n",
    "\n",
    "To view the possible parameters, either use the `help` built-in function or evaluate the newly instantiated model, as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# help(lr)\n",
    "lr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 3:** Fit the model with data (aka \"model training\").\n",
    "\n",
    "- Model is \"learning\" the relationship between X and y in our \"training data.\"\n",
    "- Process through which learning occurs varies by model.\n",
    "- Occurs in-place."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "lr.fit(X, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Once a model has been fit with data, it's called a \"fitted model.\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Step 4:** Predict the response for a new observation.\n",
    "\n",
    "- New observations are called \"out-of-sample\" data.\n",
    "- Uses the information it learned during the model training process."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Per future warning, one-dimensional arrays must be reshaped using the following.\n",
    "lr.predict(np.array([0]).reshape(1,-1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Returns a NumPy array, and we keep track of what the numbers \"mean.\"\n",
    "- Can predict for multiple observations at once."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's ask the model to make two predictions, one when the `temp` is 0 and another when the `temp` is 10. To do this, our feature matrix is always a 2-D array where each row is a list of features. Since we only have a single feature, the temperature, each row will contain only a single value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "X_new = [[0], [10]]\n",
    "lr.predict(X_new)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What we just predicted using our model is, \"If the temperature is 0 degrees, the total number of bike rentals will be ~6.046, and if the temperature is 10 degrees the total number of bike rentals will ~97.751.\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"build-a-linear-regression-model\"></a>\n",
    "## Build a Linear Regression Model\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Instantiate and fit a `LinearRegression` model on X and y from the `linear_model` section of scikit-learn."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Import, instantiate, fit.\n",
    "from sklearn.linear_model import LinearRegression\n",
    "linreg = LinearRegression()\n",
    "linreg.fit(X, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Explore the intercept and coefficients of the linear model\n",
    "\n",
    "You can search for \"sklearn linear regression\" and explore the attributes section of the documentation to learn how to do this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Print the coefficients.\n",
    "print(linreg.intercept_)\n",
    "print(linreg.coef_)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Interpreting the intercept ($\\beta_0$):\n",
    "\n",
    "- It is the value of $y$ when all independent variables are 0.\n",
    "- Here, it is the estimated number of rentals when the temperature is 0 degrees Celsius.\n",
    "- **Note:** It does not always make sense to interpret the intercept. (Why?)\n",
    "\n",
    "Interpreting the \"temp\" coefficient ($\\beta_1$):\n",
    "\n",
    "- **Interpretation:** An increase of 1 degree Celcius is _associated with_ increasing the number of total rentals by $\\beta_1$.\n",
    "- Here, a temperature increase of 1 degree Celsius is _associated with_ a rental increase of 9.17 bikes.\n",
    "- This is not a statement of causation.\n",
    "- $\\beta_1$ would be **negative** if an increase in temperature was associated with a decrease in total rentals.\n",
    "- $\\beta_1$ would be **zero** if temperature is not associated with total rentals."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"using-the-model-for-prediction\"></a>\n",
    "## Using the Model for Prediction\n",
    "---\n",
    "\n",
    "While plenty of insight can be found in reading coefficients, the most common uses of data science focus on prediction. In scikit-learn we can make predictions from a fitted model using `.predict()`, but we will also go through the calculation by hand to understand it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### How many bike rentals would we predict if the temperature was 25 degrees Celsius?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Manually calculate the prediction.\n",
    "linreg.intercept_ + linreg.coef_*25"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Use the predict method.\n",
    "linreg.predict(25)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"evaluation-metrics-for-regression-problems\"></a>\n",
    "### Evaluation Metrics for Regression Problems\n",
    "\n",
    "Evaluation metrics for classification problems, such as accuracy, are not useful for regression problems. We need evaluation metrics designed for comparing continuous values.\n",
    "\n",
    "Here are three common evaluation metrics for regression problems:\n",
    "\n",
    "**Mean absolute error (MAE)** is the mean of the absolute value of the errors:\n",
    "\n",
    "$$\\frac 1n\\sum_{i=1}^n|y_i-\\hat{y}_i|$$\n",
    "\n",
    "**Mean squared error (MSE)** is the mean of the squared errors:\n",
    "\n",
    "$$\\frac 1n\\sum_{i=1}^n(y_i-\\hat{y}_i)^2$$\n",
    "\n",
    "**Root mean squared error (RMSE)** is the square root of the mean of the squared errors:\n",
    "\n",
    "$$\\sqrt{\\frac 1n\\sum_{i=1}^n(y_i-\\hat{y}_i)^2}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Example true and predicted response values\n",
    "true = [10, 7, 5, 5]\n",
    "pred = [8, 6, 5, 10]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Calculate MAE, MSE, and RMSE using imports from sklearn metrics and NumPy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Calculate these metrics by hand!\n",
    "from sklearn import metrics\n",
    "import numpy as np\n",
    "print('MAE:', metrics.mean_absolute_error(true, pred))\n",
    "print('MSE:', metrics.mean_squared_error(true, pred))\n",
    "print('RMSE:', np.sqrt(metrics.mean_squared_error(true, pred)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's compare these metrics:\n",
    "\n",
    "- MAE is the easiest to understand, because it's the average error.\n",
    "- MSE is more popular than MAE, because MSE \"punishes\" larger errors, which tends to be useful in the real world.Also, MSE is continuous and differentiable, making it easier to use than MAE for optimization.\n",
    "- RMSE is even more popular than MSE, because RMSE is interpretable in the \"y\" units.\n",
    "\n",
    "All of these are **loss functions**, because we want to minimize them.\n",
    "\n",
    "Here's an additional example, to demonstrate how MSE/RMSE punishes larger errors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Same true values as above\n",
    "true = [10, 7, 5, 5]\n",
    "\n",
    "# New set of predicted values\n",
    "pred = [10, 7, 5, 13]\n",
    "\n",
    "# MAE is the same as before.\n",
    "print('MAE:', metrics.mean_absolute_error(true, pred))\n",
    "\n",
    "# MSE and RMSE are larger than before.\n",
    "print('MSE:', metrics.mean_squared_error(true, pred))\n",
    "print('RMSE:', np.sqrt(metrics.mean_squared_error(true, pred)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "season_dummies = pd.get_dummies(bikes.season, prefix='season')\n",
    "season_dummies.drop(season_dummies.columns[0], axis=1, inplace=True)\n",
    "# Concatenate the original DataFrame and the dummy DataFrame (axis=0 means rows, axis=1 means columns).\n",
    "bikes_dummies = pd.concat([bikes, season_dummies], axis=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"bonus-material-regularization\"></a>\n",
    "## Bonus Material: Regularization\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Regularization is a method for \"constraining\" or \"regularizing\" the size of the coefficients, thus \"shrinking\" them toward zero.\n",
    "- It reduces model variance and thus minimizes overfitting.\n",
    "- If the model is too complex, it tends to reduce variance more than it increases bias, resulting in a model that is more likely to generalize.\n",
    "\n",
    "Our goal is to locate the optimum model complexity, and thus regularization is useful when we believe our model is too complex."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"how-does-regularization-work\"></a>\n",
    "### How Does Regularization Work?\n",
    "\n",
    "For a normal linear regression model, we estimate the coefficients using the least squares criterion, which minimizes the residual sum of squares (RSS)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For a regularized linear regression model, we minimize the sum of RSS and a \"penalty term\" that penalizes coefficient size.\n",
    "\n",
    "**Ridge regression** (or \"L2 regularization\") minimizes: $$\\text{RSS} + \\alpha \\sum_{j=1}^p \\beta_j^2$$\n",
    "\n",
    "**Lasso regression** (or \"L1 regularization\") minimizes: $$\\text{RSS} + \\alpha \\sum_{j=1}^p |\\beta_j|$$\n",
    "\n",
    "- $p$ is the number of features.\n",
    "- $\\beta_j$ is a model coefficient.\n",
    "- $\\alpha$ is a tuning parameter:\n",
    "    - A tiny $\\alpha$ imposes no penalty on the coefficient size, and is equivalent to a normal linear regression model.\n",
    "    - Increasing the $\\alpha$ penalizes the coefficients and thus shrinks them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"lasso-and-ridge-path-diagrams\"></a>\n",
    "### Lasso and Ridge Path Diagrams\n",
    "\n",
    "A larger alpha (toward the left of each diagram) results in more regularization:\n",
    "\n",
    "- Lasso regression shrinks coefficients all the way to zero, thus removing them from the model.\n",
    "- Ridge regression shrinks coefficients toward zero, but they rarely reach zero.\n",
    "\n",
    "Source code for the diagrams: [Lasso regression](http://scikit-learn.org/stable/auto_examples/linear_model/plot_lasso_lars.html) and [Ridge regression](http://scikit-learn.org/stable/auto_examples/linear_model/plot_ridge_path.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Lasso and Ridge Coefficient Plots](assets/lasso_ridge_path.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"advice-for-applying-regularization\"></a>\n",
    "### Advice for Applying Regularization\n",
    "\n",
    "**Should features be standardized?**\n",
    "\n",
    "- Yes, because otherwise, features would be penalized simply because of their scale.\n",
    "- Also, standardizing avoids penalizing the intercept, which wouldn't make intuitive sense.\n",
    "\n",
    "**How should you choose between lasso regression and ridge regression?**\n",
    "\n",
    "- Lasso regression is preferred if we believe many features are irrelevant or if we prefer a sparse model.\n",
    "- Ridge can work particularly well if there is a high degree of multicollinearity in your model.\n",
    "- If model performance is your primary concern, it is best to try both.\n",
    "- Elastic net regression is a combination of lasso regression and ridge Regression."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"ridge-regression\"></a>\n",
    "### Ridge Regression\n",
    "\n",
    "- [Ridge](http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Ridge.html) documentation\n",
    "- **alpha:** must be positive, increase for more regularization\n",
    "- **normalize:** scales the features (without using StandardScaler)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Include dummy variables for season in the model.\n",
    "feature_cols = ['temp',  'season_2', 'season_3', 'season_4', 'humidity']\n",
    "X = bikes_dummies[feature_cols]\n",
    "y = bikes_dummies.total_rentals"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# alpha=0 is equivalent to linear regression.\n",
    "from sklearn.linear_model import Ridge\n",
    "\n",
    "# Instantiate the model.\n",
    "#(Alpha of zero has no regularization strength, essentially a basic linear regression.)\n",
    "ridgereg = Ridge(alpha=0, normalize=True)\n",
    "\n",
    "# Fit the model.\n",
    "ridgereg.fit(X_train, y_train)\n",
    "\n",
    "# Predict with fitted model.\n",
    "y_pred = ridgereg.predict(X_test)\n",
    "print(np.sqrt(metrics.mean_squared_error(y_test, y_pred)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Coefficients for a non-regularized linear regression\n",
    "list(zip(feature_cols, ridgereg.coef_))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To interpret these coefficients we need to convert them back to original units, which is a reason to do normalization by hand. However, in this form the coefficients have a special meaning. The intercept is now the average of our outcome, and the magnitude of each coefficient in the model is a measure of how important it is in the model. We call this feature importance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Try alpha=0.1.\n",
    "ridgereg = Ridge(alpha=0.1, normalize=True)\n",
    "ridgereg.fit(X_train, y_train)\n",
    "y_pred = ridgereg.predict(X_test)\n",
    "print(np.sqrt(metrics.mean_squared_error(y_test, y_pred)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Examine the coefficients.\n",
    "list(zip(feature_cols, ridgereg.coef_))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While the MSE barely improved, we can see there are significant changes in the weight of our coefficients.  Particularly `season_2` whose coefficient has greatly decreased toward 0.\n",
    "\n",
    "Fitting and using a Lasso Regression in scikit-learn is very similar.\n",
    "\n",
    "In addition to the typical [lasso](http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Lasso.html) and [ridge](http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Ridge.html) there is a third type of regression, [Elastic Net](http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.ElasticNet.html) which combines the penalties of the ridge and lasso methods."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"comparing-linear-regression-with-other-models\"></a>\n",
    "## Comparing Linear Regression With Other Models\n",
    "\n",
    "Advantages of linear regression:\n",
    "\n",
    "- Simple to explain.\n",
    "- Highly interpretable.\n",
    "- Model training and prediction are fast.\n",
    "- No tuning is required (excluding regularization).\n",
    "- Features don't need scaling.\n",
    "- Can perform well with a small number of observations.\n",
    "- Well understood.\n",
    "\n",
    "Disadvantages of linear regression:\n",
    "\n",
    "- Presumes a linear relationship between the features and the response.\n",
    "- Performance is (generally) not competitive with the best supervised learning methods due to high bias.\n",
    "- Can't automatically learn feature interactions."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
