## DAT NY 41- lesson 05 Resources

Bonus Material for Class

### P-values & CI
[Khan Academy Video](https://www.khanacademy.org/math/statistics-probability/significance-tests-one-sample/tests-about-population-mean/v/hypothesis-testing-and-p-values)  
[Book](http://www.larrygonick.com/titles/science/the-cartoon-guide-to-statistics/)  



### Read before next class
[Linear Regression pages 59 to 102](http://www-bcf.usc.edu/~gareth/ISL/ISLR%20First%20Printing.pdf)  
