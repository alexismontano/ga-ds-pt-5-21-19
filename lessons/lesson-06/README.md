# Dataset sources

* https://www.kaggle.com/datasets

* https://opendata.cityofnewyork.us/

* http://data.gov 

* http://www.census.gov/data.html

* http://aws.amazon.com/datasets

* http://developer.nytimes.com/docs

* https://github.com/caesar0301/awesome-public-datasets

* http://archive.ics.uci.edu/ml/index.php

* http://www.kdnuggets.com/datasets/index.html
