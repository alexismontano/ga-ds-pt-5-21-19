

Optional:

 * Read [A tutorial on Principal Components Analysis](http://www.cs.otago.ac.nz/cosc453/student_tutorials/principal_components.pdf) for a very friendly introduction that starts from the very basics.
 * Read the [Stanford PCA Tutorial](http://ufldl.stanford.edu/wiki/index.php/PCA), which is just slightly mathier.
 * Read this [step-by-step walk-through](http://sebastianraschka.com/Articles/2014_pca_step_by_step.html) of PCA with Python.
 * Read an excellent paper on [The Fundamental Theorem of Linear Algebra](http://home.eng.iastate.edu/~julied/classes/CE570/Notes/strangpaper.pdf) which goes through in an intuitive way just what SVD is all about.
 * Watch the [Chapter 10 lecture videos](http://www.dataschool.io/15-hours-of-expert-machine-learning-videos/) from *An Introduction to Statistical Learning* on PCA.


### Questions

 * Why would we prefer not to have correlated features? For OLS regression, what happens when two features are identical? What happens when they're almost identical (highly correlated)?
 * What are the possible costs/benefits of having more features rather than fewer? What is a "good" number of features?
 * What other thoughts, comments, concerns, and questions do you have? What's on your mind?


### After

Optional:

 * Learn about [random projections](http://users.ics.aalto.fi/ella/publications/randproj_kdd.pdf) and try them [in sci-kit](http://scikit-learn.org/stable/modules/random_projection.html).
 * Learn about t-Distributed Stochastic Neighbor Embedding ([t-SNE](http://homepage.tudelft.nl/19j49/t-SNE.html)), another technique that can be great for making complex data visualizable. It helped [win](http://blog.kaggle.com/2012/11/02/t-distributed-stochastic-neighbor-embedding-wins-merck-viz-challenge/) a Kaggle visualization contest, for example.
