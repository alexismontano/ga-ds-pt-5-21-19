# ![](https://ga-dash.s3.amazonaws.com/production/assets/logo-9f88ae6c9c3871690e33280fcf557f33.png) Final Project, Part 1: Lightning Talk


### PROMPT

In the field of data science, projects are practical. A good project is manageable and relates to your working domain; however, it can be hard to filter and scope good ideas when you're new to a field. That's where Part 1 of your final project comes in. 

One of the best ways to test expectations and get feedback is to share your ideas with others. For part one of your final project, you'll come up with a few different ideas you could potentially solve with data, then present your favorite in 90 seconds in front of the class. 

You'll get the opportunity to get immediate feedback and guidance in order to help you choose an awesome final project idea. Keep in mind that it's important to run though this ideation process a few times with different ideas to clarify what your project should focus on. 

**Goal**: Prepare a 90 second lightning talk that covers 1 potential project topic.

---

### DELIVERABLES

#### Lightning Presentation

- **Requirements:** You will submit 3 specific aims in the form of word/text doc on google drive that goes over your project ideas. In class, you will present a 90 second talk that covers 1 specific aim (no decks!). It should explain the following:
    * ___The Problem___: What's the background and scope of the project idea? What problem are you attempting to address or solve? Who may it matter to?
    * ___Data___: What data exists to help solve this problem? Where is it coming from? What does the data look like? What is the observation?
    * ___Hypotheses___: Given the problem and data you're aware of, what do you believe is the solution? What does success look like?

    
- **Detailed Breakdown:** Your presentation must:
    * Explain one potential project, including: a problem statement, a hypothesis, and potential data set
    * Be 90 seconds in duration
    * Demonstrate familiarity with the domain of the data

- **Bonus:**
    - Cite similar/previous work that will better inform your strategy
    - Present a potential model to tackle your specific aim 

- **Submission:**	
    * Present talk in class
    * Submit doc with 3 aims/project ideas to Google drive.

---

### TIMELINE

| Deadline | Deliverable| Description |
|:-:|---|---|
| Lesson 8 |  Part 1 - Lightning Presentation  | Present 3 Problem Statements   |
| Lesson 10 | Part 2 - Experiment Writeup  |  Research Design Problem Statement & Outline   |
| Lesson 14 | Part 3 - Exploratory Analysis  | Dataset Approval and Exploratory Analysis   |
| Lesson 16 | Part 4 - Notebook Draft  |  Jupyter Notebook & Model Draft  |
| Lesson 19 | Part 5 - Presentation  | Present Your Final Project   |

---

### EVALUATION

Your project will be assessed using the following standards:

1. Identify the Problem

#### Rubric: [Click here for the complete rubric](./final-project-1-rubric.md).

Requirements for these standards will be assessed using the scale below:

    Score | Expectations
    ----- | ------------
    **0** | _Incomplete._
    **1** | _Does not meet expectations._
    **2** | _Meets expectations, good job!_

While your total score may serve as a helpful gauge of whether you've met project goals, __specific standards scores are more important__ since they can show you where to focus your efforts in the future!

---

### RESOURCES

#### Suggestions for Getting Started

- Think about what you want to learn most from this project
- Write several specific aims for topics you find interesting that will help you reach your learning goal 
- Draw a starting conceptual model to help you identify what you would need for one of those aims 
- Look for data that will provide the features you need for your aim
- Re-write your aims based on the "real-world" data you have available
- Think about this as an elevator pitch: be prepared, do some light research, but don't go down any "rabbit holes".
- Think about how much time you can dedicate to the project each week, and where you think the largest challenges will lie. 
- Make a project plan with intermediary deadlines for yourself

#### Additional Links

- For overall project ideas, consider looking at former [General Assembly Data Science Projects](https://gallery.generalassemb.ly/DS?metro=)
