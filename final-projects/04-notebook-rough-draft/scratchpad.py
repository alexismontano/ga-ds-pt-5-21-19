# Import libraries I be using
import pandas as pd
import numpy as np
import os.path
import matplotlib.pyplot as plt

# Import file to the data frame
fileLocation = "C:\\Users\\Alexis\\OneDrive\\Bitbucket\\GA-DS-PT-5-21-19\\final-projects\\data\\zip_zhvi_all_homes.csv"
df = pd.read_csv(fileLocation, low_memory=False)

# Organize all values based on RegionID 
df = df.sort_values(by=['RegionID'])

# Create Index for the data frame base on Regions IDs
#index_ = df['RegionID']

# Set the RegionID as index
#df.index = index_

# RegionID	RegionName	City	State	Metro	CountyName	SizeRank
data = df[['RegionID', 'RegionName', 'City', 'State', 'Metro', 'CountyName', 'SizeRank']]

# Drop columns not part of the transpose
df_to_melt = df.drop(['RegionName', 'City', 'State', 'Metro', 'CountyName', 'SizeRank'], axis=1, inplace=False)

# Un-Pivot data base on the Region ID
df_melt = pd.melt(df_to_melt, id_vars='RegionID')

# Rename new columns variable and value to their proper names year_month, home_value
df_melt = df_melt.rename(index=str, columns={"variable":"year_month", "value":"home_value"})

# Join the data and df_melt back together to data
data = data.merge(df_melt, how='inner')

# Change the data type for year_month to a panda date type
data['year_month'] = pd.to_datetime(data['year_month'])

# Test Print Statement
print(data.head())