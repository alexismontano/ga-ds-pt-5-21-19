#%% Change working directory from the workspace root to the ipynb file location. Turn this addition off with the DataScience.changeDirOnImportExport setting
# ms-python.python added
import os
try:
	os.chdir(os.path.join(os.getcwd(), 'final-projects\\04-notebook-rough-draft'))
	print(os.getcwd())
except:
	pass
#%% [markdown]
# # Final Project Exploratory Analysis
# ### Author By: Alexis M Montano

#%%
# Import libraries I be using
import pandas as pd
import numpy as np
import os.path
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


#%%
# Import file to the data frame
fileLocation = "C:\\Users\\Alexis\\OneDrive\\Bitbucket\\GA-DS-PT-5-21-19\\final-projects\\data\\zip_zhvi_all_homes.csv"
df = pd.read_csv(fileLocation, low_memory=False)


#%%
# Select top five head records
df.head(5)


#%%
# Length of the dataframe
len(df)


#%%
# Count number of items null
df.isnull().sum()


#%%
# Drop columns not been consider
df.drop(["Metro", "SizeRank"], axis=1, inplace=True)
df.columns


#%%
# Filter down data to Queens County
df2 = df[(df["CountyName"]=="Queens County")]
df2.head(25)


#%%
# The shape of the DataFrame
df2.shape


#%%
# Are they null values in the Queens data set
df2.isnull().sum()


#%%
# The Mean home value for the last month in the data set?

df2[df2.columns[-1]].mean()


#%%
# The median home value for the last month in the data set?
df2[df2.columns[-1]].median()


#%%
# The minimum home value for the last month in the data set?
df2[df2.columns[-1]].min()


#%%
# The maximum home value for the last month in the data set?
df2[df2.columns[-1]].max()


#%%
# Histogram of Queens County data last month in the data set?
df2[df2.columns[-1]].hist()


#%%
# Boxplot of Queens County data last month in the data set?
df2.boxplot(column=[df2.columns[-1]])


#%%
# visualize the relationship between the city and home values in the last month, year, five, ten and fifteen years
fig, axs = plt.subplots(1, 5, sharey=True)
df2.plot(kind='scatter', x='RegionName', y=df2.columns[-1], ax=axs[0], figsize=(16, 8))
df2.plot(kind='scatter', x='RegionName', y=df2.columns[-12], ax=axs[1])
df2.plot(kind='scatter', x='RegionName', y=df2.columns[-60], ax=axs[2])
df2.plot(kind='scatter', x='RegionName', y=df2.columns[-120], ax=axs[3])
df2.plot(kind='scatter', x='RegionName', y=df2.columns[-180], ax=axs[4])


#%%
# Home values Standard deviation
df2[df2.columns[-1]].std()


#%%
# The top 10 lowest price homes?
df2[df2.columns[-1]].sort_values(ascending=True).head(10)


#%%
# The top 10 highest price homes?
df2[df2.columns[-1]].sort_values(ascending=False).head(10)


