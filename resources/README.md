### Linear Regression 
[Basics Pages 59-102](http://www-bcf.usc.edu/~gareth/ISL/ISLR%20First%20Printing.pdf)  
[Model Selection 203-259 e.g. Ridge/Lasso](http://www-bcf.usc.edu/~gareth/ISL/ISLR%20First%20Printing.pdf)  

### Cross-Validation
[Basics Pages 175-187](http://www-bcf.usc.edu/~gareth/ISL/ISLR%20First%20Printing.pdf)  
[Arlot, 2010- Survey of cross-validation](https://projecteuclid.org/download/pdfview_1/euclid.ssu/1268143839)  
[Zhang, 1993- Model Selection via multifold cross validation](http://www.jstor.org/stable/3035592?seq=1#page_scan_tab_contents)  
[Domingos- Useful things](https://homes.cs.washington.edu/~pedrod/papers/cacm12.pdf)  

### K-NN
[Basics Pages 175-187](http://www-bcf.usc.edu/~gareth/ISL/ISLR%20First%20Printing.pdf)  
[Review](http://cs231n.github.io/classification/#nn)  
[Cunningham, 2007](https://www.researchgate.net/profile/Sarah_Delany/publication/228686398_k-Nearest_neighbour_classifiers/links/0fcfd50d0c1d1f41ad000000/k-Nearest-neighbour-classifiers.pdf)  

### Logistic Regression
[Basics Pages 127-151](http://www-bcf.usc.edu/~gareth/ISL/ISLR%20First%20Printing.pdf)  
[Logistic Regression- deeper dive](http://repository.cmu.edu/cgi/viewcontent.cgi?article=1217&context=robotics)

### Ensemble Methods Overview
[Ensemble Methods](https://books.google.com/books?hl=en&lr=&id=MFzRBQAAQBAJ&oi=fnd&pg=PP1&ots=mJujoCeSDn&sig=NdIWgaP3z59a5axCjbKtGbZh68E#v=onepage&q&f=false)  

### Random Forests
[Basics Pages 303-332](http://www-bcf.usc.edu/~gareth/ISL/ISLR%20First%20Printing.pdf)  
[Breiman, 2001- original paper](https://www.stat.berkeley.edu/~breiman/randomforest2001.pdf)  
[Strobl, 2007- bias in variable importance measures](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-8-25)  
[Diettererich, 2000- Ensemble Methods in Machine Learning](https://link.springer.com/chapter/10.1007/3-540-45014-9_1)

[Beware Default Random Forest Importances](http://explained.ai/rf-importance/index.html)

### LDA
[Blei, 2003](http://www.jmlr.org/papers/volume3/blei03a/blei03a.pdf)

### Word2Vec
[Mikolov](https://papers.nips.cc/paper/5021-distributed-representations-of-words-and-phrases-and-their-compositionality.pdf)

